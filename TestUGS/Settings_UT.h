#ifndef __CSETTINGS_UT_H__
#define __CSETTINGS_UT_H__

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <vector>

//============================================================================
//		CSettingsTest

class CSettingsTest : public ::testing::Test
{
protected:
	struct SMetadata
	{
		WORD   nMaxLength;     ///< ������ ������ ������ (���������+������)
		WORD   nType;          ///< ��� ������
		WORD   nDestination;   ///< �������
		WORD   nSource;        ///< ��������
		DWORD  nTimeStamp;     ///< ������� �������
		unsigned char chIndex; ///< �����-�� ������, ��. CSettings::UpdateStatusMsg()
		unsigned char chUnk;   ///< ��� ��� - �� ����, �� ��� ����-�� ����� :)
		unsigned char _;       ///< �������� ��� offsetfo( ... )
//		WORD   nCRC16          ///< CRC-���

		SMetadata( void )
			: nMaxLength( 0 ), nType( 0 ), nDestination( 0 ), nSource( 0 )
			, nTimeStamp( 0 ), chIndex( 0 ), chUnk( 0 ), _( 0 )
		{
		}
	};

protected:
	unsigned short GetMessageTimeOffset( void ) const
	{
		return offsetof( SMetadata, nTimeStamp ); //8;
	}
	unsigned short GetMessageIndexOffset( void ) const
	{
		return offsetof( SMetadata, chIndex ); //12;
	}

	unsigned short GetStatusTimeOffset( void ) const
	{
		return GetSpaceSizeForHeader() + offsetof( SMetadata, nTimeStamp ); //22;
	}
	unsigned short GetStatusIndexOffset( void ) const
	{
		return GetSpaceSizeForHeader() + offsetof( SMetadata, chIndex ); //26;
	}

	unsigned short GetSizeOfCRC( void ) const
	{
		return sizeof( WORD );
	}

	unsigned short GetSpaceSizeForHeader( void ) const
	{
		return offsetof( SMetadata, _ ); //14;
	}

		// ��. CSettings::UpdateStatusMsg() � CSettings::MakeStatusMsg()
	unsigned short GetSizeOfMETA( void ) const
	{
		return GetSpaceSizeForHeader() + GetSizeOfCRC();
	}

		// ��. CSettings::MakeStatusMsg()
	unsigned short GetStatusDataOffset( void ) const
	{
		return ( 2 * GetSpaceSizeForHeader() );
	}

	unsigned short GetSpaceSizeForCRC( void ) const
	{
		return ( 2 * GetSizeOfCRC() );
	}

	unsigned short GetSizeOfMessageMETA( void ) const
	{
		return GetStatusDataOffset() + GetSpaceSizeForCRC();
	}

		// CSettings::MESSAGETYPE::m_wMaxLength ����� ��� WORD
		// ������ � ��������������� maximum
	unsigned short GetStatusDataSizeMax( void ) const
	{
		return ( (std::numeric_limits< WORD >::max)() - GetSizeOfMessageMETA() );
	}

	unsigned char GetValueForUpdateStatusMsg( void ) const
	{
		return m_chValueForUpdateStatusMsg;
	}

protected:
	bool ReadFromByteArray(
		const TByteArray & rByteArray, size_t nReadPosition,
		void * pData, size_t nDataSize
	) const
	{
		if( nReadPosition + nDataSize > rByteArray.size() ) {
			EXPECT_LE( nReadPosition + nDataSize, rByteArray.size() )
				<< "Buffer size " << rByteArray.size()
				<< " Unable to read data with size " << nDataSize
				<< " starting from position " << nReadPosition
			;

			return false;
		}

		::memcpy( pData, &rByteArray[nReadPosition], nDataSize );

		return true;
	}

	template< typename T >
	bool ReadFromByteArray(
		const TByteArray & rByteArray, size_t nReadPosition,
		T & rValue
	) const
	{
		return ReadFromByteArray( rByteArray, nReadPosition, rValue, sizeof( T ) );
	}

	bool WriteToByteArray(
		TByteArray & rByteArray, size_t nWritePosition,
		const void * pData, size_t nDataSize
	) const
	{
		if( nWritePosition + nDataSize > rByteArray.size() ) {
			EXPECT_LE( nWritePosition + nDataSize, rByteArray.size() )
				<< "Buffer size " << rByteArray.size()
				<< " Unable to write data with size " << nDataSize
				<< " starting from position " << nWritePosition
			;

			return false;
		}

		::memcpy( &rByteArray[nWritePosition], pData, nDataSize );

		return true;
	}

	template< typename T >
	bool WriteToByteArray(
		TByteArray & rByteArray, size_t nWritePosition,
		const T & rValue
	) const
	{
		return WriteToByteArray( rByteArray, nWritePosition, &rValue, sizeof( T ) );
	}

protected:
	virtual void SetUp( void )
	{
		m_chValueForUpdateStatusMsg = 'x';

		std::vector< BYTE > data;
		for( BYTE n = 129; n > 0; --n ) {
			data.push_back( n );
		}
		m_TestSample.SetStatusData( data );
	}

protected:
	CSettings      m_TestSample;
	TMsgTypeSet    m_DefaultMsgTypeSet = {0x0000};
	unsigned char  m_chValueForUpdateStatusMsg;
};

//============================================================================


#endif // __CSETTINGS_UT_H__
