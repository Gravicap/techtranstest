#include "../StdAfx.h"
#include "Settings_UT.h"

#include <algorithm>
#include <list>

//----------------------------------------------------------------------------

::testing::AssertionResult AssertEqualOneOfTwo(
	const char * val_expr, const char * _1st_expr, const char * _2nd_expr,
	WORD val, WORD _1st, WORD _2nd
)
{
	if( val != _1st ) {
		if( val != _2nd ) {
			return ::testing::AssertionFailure() << std::endl
				<< "Expected equality of" << std::endl
				<< "  " << val_expr << std::endl << "    which is: " << val << std::endl
				<< "to one of the values:" << std::endl
				<< "  " << _1st_expr << std::endl << "    which is " << _1st << std::endl
				<< "  " << _2nd_expr << std::endl << "    which is " << _2nd
			;
		}
	}

	return ::testing::AssertionSuccess();
}

#define EXPECT_EQ_1OF2( val, _1, _2 ) \
	EXPECT_PRED_FORMAT3( AssertEqualOneOfTwo, val, _1, _2 )
#define ASSERT_EQ_1OF2( val, _1, _2 ) \
	ASSERT_PRED_FORMAT3( AssertEqualOneOfTwo, val, _1, _2 )

//----------------------------------------------------------------------------

::testing::AssertionResult AssertEqualArrays(
	const char * lhs_expr, const char * rhs_expr,
	const TByteArray & lhs_array, const TByteArray & rhs_array
)
{
	::testing::Message msg;
	msg << "Expect equlity of arrays:" << std::endl
		<< "  " << lhs_expr << std::endl
		<< "  " << rhs_expr << std::endl
	;

	if( lhs_array.size() != rhs_array.size() ) {
		msg << "but they differ in size: " << lhs_array.size() << " vs " << rhs_array.size();
		return ::testing::AssertionFailure() << msg;
	}

	for( TByteArray::size_type n = 0; n < lhs_array.size(); ++n ) {
		if( lhs_array[n] != rhs_array[n] ) {
			msg << "but they differ by item:" << std::endl
				<< "  " << lhs_expr << "[" << n << "] is 0x"
				<< ::testing::internal::String::FormatByte( lhs_array[n] ) << std::endl
				<< "  " << rhs_expr << "[" << n << "] is 0x"
				<< ::testing::internal::String::FormatByte( rhs_array[n] )
			;
			return ::testing::AssertionFailure() << msg;
		}
	}

	return ::testing::AssertionSuccess();
}

#define EXPECT_EQ_ARRAYS( arr_1, arr_2 ) \
	EXPECT_PRED_FORMAT2( AssertEqualArrays, arr_1, arr_2 )
#define ASSERT_EQ_ARRAYS( arr_1, arr_2 ) \
	ASSERT_PRED_FORMAT2( AssertEqualArrays, arr_1, arr_2 )

//----------------------------------------------------------------------------

TEST_F( CSettingsTest, UpdateStatusMsg )
{
	// ��-��������, ����� ����� ��������� �������������� ���� �������� �����
	// ������� CSettings ����� ���, �� ������� ������ �������� ���������� ������
	// UpdateStatusMsg().

	// ���������� ���������, � ������� ����� ��������
	m_TestSample.MakeStatusMsg();

	// �������� ������, � �������� ����� ��������� ���������
	const auto nKeptCRC16Init = m_TestSample.GetCRC16Init();
	TByteArray arrOrigStatusMsg = m_TestSample.GetStatusMessageData();

	// ������ m_arStatusMsg �� ����� ���� ����� GetSizeOfMessageMETA(),
	// ����� ����� ������ ������ �\��� access violation
	ASSERT_GE(
		arrOrigStatusMsg.size(),
		m_TestSample.GetStatusData().size() + GetSizeOfMessageMETA()
	);

	// �����
	m_TestSample.UpdateStatusMsg( GetValueForUpdateStatusMsg() );

	const TByteArray arrTestStatusMsg = m_TestSample.GetStatusMessageData();

	// ������ �������� ������ �� ������ ����������.
	// ����� ����������� �������� ����� �������� � access violation.
	ASSERT_EQ( arrTestStatusMsg.size(), arrOrigStatusMsg.size() );

	// �������� ��� ������� CRC �� ������ ����������
	EXPECT_EQ( nKeptCRC16Init, m_TestSample.GetCRC16Init() );

	SMetadata sStatusMETA, sMessageMETA, sMessageMETA___;

	// ��������� ������������ ���������� ���� � ����������

	ASSERT_TRUE( ReadFromByteArray(
		arrTestStatusMsg, 0,
		&sStatusMETA, GetSpaceSizeForHeader()
	) );
	ASSERT_TRUE( ReadFromByteArray(
		arrTestStatusMsg, GetSpaceSizeForHeader(),
		&sMessageMETA, GetSpaceSizeForHeader()
	) );

	EXPECT_EQ( sStatusMETA.chIndex, GetValueForUpdateStatusMsg() );
	EXPECT_EQ( sMessageMETA.chIndex, GetValueForUpdateStatusMsg() );
	EXPECT_EQ( sStatusMETA.nTimeStamp, sMessageMETA.nTimeStamp );

	// ������ ������� � ������� �������� � ������� � �������� ��������

	ASSERT_TRUE( WriteToByteArray(
		arrOrigStatusMsg, GetMessageTimeOffset(),
		sStatusMETA.nTimeStamp
	) );
	ASSERT_TRUE( WriteToByteArray(
		arrOrigStatusMsg, GetMessageIndexOffset(),
		GetValueForUpdateStatusMsg()
	) );

	ASSERT_TRUE( WriteToByteArray(
		arrOrigStatusMsg, GetStatusTimeOffset(),
		sMessageMETA.nTimeStamp
	) );
	ASSERT_TRUE( WriteToByteArray(
		arrOrigStatusMsg, GetStatusIndexOffset(),
		GetValueForUpdateStatusMsg()
	) );

	// ��������� � ������� CRC-����

	// CRC ��� ������ ��������� ( CSettings::m_arStatusData + META )
	ASSERT_TRUE( WriteToByteArray(
		arrOrigStatusMsg,
		arrOrigStatusMsg.size() - GetSpaceSizeForCRC(),
		CRC16(
			&arrOrigStatusMsg[ GetSpaceSizeForHeader() ],
			arrOrigStatusMsg.size() - GetSpaceSizeForHeader() - GetSpaceSizeForCRC(),
			nKeptCRC16Init
		)
	) );

	// CRC ��� ��������� ������� ( CSettings::m_arStatusMsg + META )
	ASSERT_TRUE( WriteToByteArray(
		arrOrigStatusMsg,
		arrOrigStatusMsg.size() - GetSizeOfCRC(),
		CRC16(
			&arrOrigStatusMsg[ 0 ],
			arrOrigStatusMsg.size() - GetSizeOfCRC(),
			nKeptCRC16Init
		)
	) );

	// ������ ������� �������� � �������� �������

	const auto DiffPoints =
		std::mismatch( arrOrigStatusMsg.cbegin(), arrOrigStatusMsg.cend(),
			arrTestStatusMsg.cbegin() );

	const auto n = DiffPoints.first - arrOrigStatusMsg.cbegin();
	if( n != arrOrigStatusMsg.size() ) {
		EXPECT_EQ( arrOrigStatusMsg[n], arrTestStatusMsg[n] )
			<< "Diffs starts at index " << n << " "
			<< ( n < GetSpaceSizeForHeader()
				? "Status metadata was corrupted on update!"
				: n < GetStatusDataOffset()
					? "Message metadata was corrupted on update!"
					: n < arrOrigStatusMsg.size() - GetSpaceSizeForCRC()
						? "Message data was corrupted on update!"
						: n < arrOrigStatusMsg.size() - GetSizeOfCRC()
							? "Message data checksums don't match!"
							: "Status data checksums don't match!"
				)
		;
	}
}

//****************************************************************************

TEST_F( CSettingsTest, MakeStatusMsg )
{
	// ��-��������, ����� ����� ��������� �������������� ���� �������� �����
	// ������� CSettings ����� ���, �� ������� ������ �������� ���������� ������
	// MakeStatusMsg()

	// ������ ��������� �� ����� ��������� �������� GetStatusDataSizeMax(),
	// ����� ����� ������ ������ �\��� access violation
	ASSERT_LE( m_TestSample.GetStatusData().size(), GetStatusDataSizeMax() );

	const auto KeptStatusHdr = m_TestSample.GetStatusHdr();
	const auto KeptStatusMsg = m_TestSample.GetStatusMessageHdr();
	const auto KeptStatusData = m_TestSample.GetStatusData();

	// �����
	m_TestSample.MakeStatusMsg();

	// �������� ������ �� ������ ���� ����������

	EXPECT_EQ( KeptStatusHdr.m_wType, m_TestSample.GetStatusHdr().m_wType );
	EXPECT_EQ( KeptStatusHdr.m_wDestination, m_TestSample.GetStatusHdr().m_wDestination );
	EXPECT_EQ( KeptStatusHdr.m_wSource, m_TestSample.GetStatusHdr().m_wSource );

	EXPECT_EQ( KeptStatusMsg.m_wType, m_TestSample.GetStatusMessageHdr().m_wType );
	EXPECT_EQ( KeptStatusMsg.m_wDestination, m_TestSample.GetStatusMessageHdr().m_wDestination );
	EXPECT_EQ( KeptStatusMsg.m_wSource, m_TestSample.GetStatusMessageHdr().m_wSource );

	EXPECT_EQ_ARRAYS( KeptStatusData, m_TestSample.GetStatusData() )
		<< "The original status data is corrupted!";


	// ��������� ��������� �� ������������ ������

	ASSERT_LE(
		m_TestSample.GetStatusMessageData().size(),
		(std::numeric_limits< WORD >::max)()
	);
	ASSERT_GE(
		m_TestSample.GetStatusMessageData().size(),
		m_TestSample.GetStatusData().size() + GetSizeOfMessageMETA()
	);
	EXPECT_EQ(
		m_TestSample.GetStatusHdr().m_wMaxLength,
		m_TestSample.GetStatusMessageData().size()
	);
	EXPECT_EQ(
		m_TestSample.GetStatusMessageHdr().m_wMaxLength,
		m_TestSample.GetStatusData().size() + GetSizeOfMETA()
	);

	// ��������, ����� �� ��������� ������. 
	// ����� �� ����������� ������������ ��������, � ������ ������������ ������
	auto sStatusMETA
		= reinterpret_cast<const SMetadata&>(*m_TestSample.GetStatusMessageData().data());
	auto sMessageMETA
		= reinterpret_cast<const SMetadata&>(*(m_TestSample.GetStatusMessageData().data() + GetSpaceSizeForHeader()));

	EXPECT_EQ( sStatusMETA.nMaxLength, m_TestSample.GetStatusHdr().m_wMaxLength );
	EXPECT_EQ( sStatusMETA.nType, m_TestSample.GetStatusHdr().m_wType );
	EXPECT_EQ( sStatusMETA.nDestination, m_TestSample.GetStatusHdr().m_wDestination );
	EXPECT_EQ( sStatusMETA.nSource, m_TestSample.GetStatusHdr().m_wSource );

	EXPECT_EQ( sMessageMETA.nMaxLength, m_TestSample.GetStatusMessageHdr().m_wMaxLength );
	EXPECT_EQ( sMessageMETA.nType, m_TestSample.GetStatusMessageHdr().m_wType );
	EXPECT_EQ( sMessageMETA.nDestination, m_TestSample.GetStatusMessageHdr().m_wDestination );
	EXPECT_EQ( sMessageMETA.nSource, m_TestSample.GetStatusMessageHdr().m_wSource );

	// �������� ��� ������ ��������� �������� �����.
	// ���� ��������� ������, ������ ������, �� �������� ��� �������.

	if( ! std::equal( KeptStatusData.cbegin(), KeptStatusData.cend(),
		m_TestSample.GetStatusMessageData().cbegin() + GetStatusDataOffset()	)
	) {
		ADD_FAILURE() << "The status data was recorded incorrectly in to message!";
	}
}

//****************************************************************************

TEST_F( CSettingsTest, SaveAndLoad )
{
	std::string file_name = "..\\USG_Test.ini";
	CSettings SavedSettings;
	CSettings LoadedSettings;

	ASSERT_TRUE( SavedSettings.Load( "..\\UGS.ini" ) );

	ASSERT_TRUE( SavedSettings.Save( file_name.c_str() ) );
	ASSERT_TRUE( LoadedSettings.Load( file_name.c_str() ) );

	EXPECT_EQ( LoadedSettings.m_dwPollingPeriod, SavedSettings.m_dwPollingPeriod );
	EXPECT_EQ( LoadedSettings.m_bTestLoopback, SavedSettings.m_bTestLoopback );
	EXPECT_EQ( LoadedSettings.m_bShowSIOMessages, SavedSettings.m_bShowSIOMessages );
	EXPECT_EQ( LoadedSettings.m_bShowMessageErrors, SavedSettings.m_bShowMessageErrors );
	EXPECT_EQ( LoadedSettings.m_bShowCOMErrors, SavedSettings.m_bShowCOMErrors );
	EXPECT_STREQ( LoadedSettings.m_strSettingsReportPath, SavedSettings.m_strSettingsReportPath );
	EXPECT_EQ( LoadedSettings.m_nBufferSize, SavedSettings.m_nBufferSize );
	EXPECT_EQ( LoadedSettings.m_nIncomingPort, SavedSettings.m_nIncomingPort );

	EXPECT_STREQ( LoadedSettings.m_strCOMSetup, SavedSettings.m_strCOMSetup );
	EXPECT_EQ( LoadedSettings.m_iCOMRttc, SavedSettings.m_iCOMRttc );
	EXPECT_EQ( LoadedSettings.m_iCOMWttc, SavedSettings.m_iCOMWttc );
	EXPECT_EQ( LoadedSettings.m_iCOMRit, SavedSettings.m_iCOMRit );
	EXPECT_EQ( LoadedSettings.m_wCPAddr, SavedSettings.m_wCPAddr );
	EXPECT_EQ( LoadedSettings.m_wPUAddr, SavedSettings.m_wPUAddr );

	ASSERT_EQ_ARRAYS( LoadedSettings.m_arPrefix, SavedSettings.m_arPrefix );

	ASSERT_EQ_ARRAYS( LoadedSettings.m_arOutPrefix, SavedSettings.m_arOutPrefix );

	EXPECT_EQ( LoadedSettings.m_wCRC16Init, SavedSettings.m_wCRC16Init );
	EXPECT_EQ( LoadedSettings.m_wComposedType, SavedSettings.m_wComposedType );
	EXPECT_EQ(
		LoadedSettings.m_wOutputComposedType,
		SavedSettings.m_wOutputComposedType
	);

	if( LoadedSettings.m_bUnpackAll != SavedSettings.m_bUnpackAll ) {
		EXPECT_EQ( LoadedSettings.m_bUnpackAll, SavedSettings.m_bUnpackAll );
	}
	else {
		//		�� �������, ������ ��� �� ��������... Conditional ���� � man'�
		//	EXPECT_THAT(
		//		LoadedSettings.m_mapMsgTypesToUnpack,
		//		::testing::Conditional(
		//			LoadedSettings.m_bUnpackAll,
		//			m_DefaultMsgTypeSet,
		//			SavedSettings.m_mapMsgTypesToUnpack
		//		)
		//	);


		if( LoadedSettings.m_bUnpackAll ) {
			EXPECT_EQ(
				LoadedSettings.m_mapMsgTypesToUnpack,
				m_DefaultMsgTypeSet
			);
		}
		else {
			EXPECT_EQ(
				SavedSettings.m_mapMsgTypesToUnpack,
				LoadedSettings.m_mapMsgTypesToUnpack
			);
		}
	}

	EXPECT_EQ(
		LoadedSettings.m_MarkComposedMask.m_wDestMask,
		SavedSettings.m_MarkComposedMask.m_wDestMask
	);
	EXPECT_EQ(
		LoadedSettings.m_MarkComposedMask.m_wSrcMask,
		SavedSettings.m_MarkComposedMask.m_wSrcMask
	);
	EXPECT_EQ(
		LoadedSettings.m_MarkNestedMask.m_wDestMask,
		SavedSettings.m_MarkNestedMask.m_wDestMask
	);
	EXPECT_EQ(
		LoadedSettings.m_MarkNestedMask.m_wSrcMask,
		SavedSettings.m_MarkNestedMask.m_wSrcMask
	);

	if( LoadedSettings.m_bMarkAll != SavedSettings.m_bMarkAll ) {
		EXPECT_EQ( LoadedSettings.m_bMarkAll, SavedSettings.m_bMarkAll );
	}
	else {
		if( LoadedSettings.m_bMarkAll ) {
			EXPECT_EQ(
				LoadedSettings.m_mapMsgTypesToMark,
				m_DefaultMsgTypeSet
			);
		}
		else {
			EXPECT_EQ(
				SavedSettings.m_mapMsgTypesToMark,
				LoadedSettings.m_mapMsgTypesToMark
			);
		}
	}

	if( LoadedSettings.m_mapMsgTypes.size() != SavedSettings.m_mapMsgTypes.size() ) {
		EXPECT_EQ(
			LoadedSettings.m_mapMsgTypes.size(),
			SavedSettings.m_mapMsgTypes.size()
		);
	}
	else {
		auto it = SavedSettings.m_mapMsgTypes.cbegin();
		while( it != SavedSettings.m_mapMsgTypes.end()) {
			auto wExpectedKey = it->first;
			++it;
			if( LoadedSettings.m_mapMsgTypes.find( wExpectedKey ) == LoadedSettings.m_mapMsgTypes.end()) {
				ADD_FAILURE() << "Expected type 0x"
					<< ::testing::internal::String::FormatHexInt( wExpectedKey )
					<< " is missing in LoadedSettings.m_mapMsgTypes";
				continue;
			}

			EXPECT_EQ( LoadedSettings.m_mapMsgTypes[ wExpectedKey ].m_wType, wExpectedKey );

			if( 0x0001 == wExpectedKey ) {
				// ���� ��� ��������� ��������������� ��������

				EXPECT_EQ( LoadedSettings.m_mapMsgTypes[ 0x0001 ].m_wMaxLength, 0x1000 );
				EXPECT_EQ( LoadedSettings.m_mapMsgTypes[ 0x0001 ].m_wSource, 0 );
				EXPECT_EQ( LoadedSettings.m_mapMsgTypes[ 0x0001 ].m_wDestination, 0 );
				EXPECT_EQ( LoadedSettings.m_mapMsgTypes[ 0x0001 ].m_wSrcMask, 0 );
				EXPECT_EQ( LoadedSettings.m_mapMsgTypes[ 0x0001 ].m_wDestMask, 0 );

				continue;
			}

			EXPECT_EQ(
				LoadedSettings.m_mapMsgTypes[ wExpectedKey ].m_wMaxLength,
				SavedSettings.m_mapMsgTypes[ wExpectedKey ].m_wMaxLength
			) << "expected key is: 0x" << ::testing::internal::String::FormatHexInt( wExpectedKey );

			switch( wExpectedKey )
			{
			case 0x0505 :
				EXPECT_EQ_1OF2(
					LoadedSettings.m_mapMsgTypes[ 0x0505 ].m_wSource,
					SavedSettings.m_mapMsgTypes[ 0x0505 ].m_wSource,
					LoadedSettings.m_wPUAddr
				);
				EXPECT_EQ_1OF2(
					LoadedSettings.m_mapMsgTypes[ 0x0505 ].m_wDestination,
					SavedSettings.m_mapMsgTypes[ 0x0505 ].m_wDestination,
					LoadedSettings.m_wCPAddr
				);
			break;
			case 0x0521 :
				EXPECT_EQ_1OF2(
					LoadedSettings.m_mapMsgTypes[ 0x0521 ].m_wSource,
					SavedSettings.m_mapMsgTypes[ 0x0521 ].m_wSource,
					LoadedSettings.m_wCPAddr
				);
				EXPECT_EQ(
					LoadedSettings.m_mapMsgTypes[ 0x0521 ].m_wDestination,
					SavedSettings.m_mapMsgTypes[ 0x0521 ].m_wDestination
				);
			break;
			case 0x0532 :
				EXPECT_EQ_1OF2(
					LoadedSettings.m_mapMsgTypes[ 0x0532 ].m_wSource,
					SavedSettings.m_mapMsgTypes[ 0x0532 ].m_wSource,
					LoadedSettings.m_wCPAddr
				);
				EXPECT_EQ(
					LoadedSettings.m_mapMsgTypes[ 0x0532 ].m_wDestination,
					SavedSettings.m_mapMsgTypes[ 0x0532 ].m_wDestination
				);
			break;
			default:
				EXPECT_EQ_1OF2(
					LoadedSettings.m_mapMsgTypes[ wExpectedKey ].m_wSource,
					SavedSettings.m_mapMsgTypes[ wExpectedKey ].m_wSource,
					LoadedSettings.m_wCPAddr
				) << "expected key is: 0x" << ::testing::internal::String::FormatHexInt( wExpectedKey );
				EXPECT_EQ_1OF2(
					LoadedSettings.m_mapMsgTypes[ wExpectedKey ].m_wDestination,
					SavedSettings.m_mapMsgTypes[ wExpectedKey ].m_wDestination,
					LoadedSettings.m_wPUAddr
				) << "expected key is: 0x" << ::testing::internal::String::FormatHexInt( wExpectedKey );
			}

			EXPECT_EQ(
				LoadedSettings.m_mapMsgTypes[ wExpectedKey ].m_wDestMask,
				SavedSettings.m_mapMsgTypes[ wExpectedKey ].m_wDestMask
			) << "expected key is: 0x" << ::testing::internal::String::FormatHexInt( wExpectedKey );
			EXPECT_EQ(
				LoadedSettings.m_mapMsgTypes[ wExpectedKey ].m_wSrcMask,
				SavedSettings.m_mapMsgTypes[ wExpectedKey ].m_wSrcMask
			) << "expected key is: 0x" << ::testing::internal::String::FormatHexInt( wExpectedKey );

		} // while( pos != NULL )
	}

	EXPECT_EQ(
		LoadedSettings.m_nStatusPeriod,
		SavedSettings.m_nStatusPeriod
	);
	EXPECT_EQ(
		LoadedSettings.m_iSendStatTO,
		SavedSettings.m_iSendStatTO
	);

	EXPECT_EQ(
		LoadedSettings.m_StatusHdr.m_wType,
		SavedSettings.m_StatusHdr.m_wType
	);
	EXPECT_EQ(
		LoadedSettings.m_StatusHdr.m_wDestination,
		SavedSettings.m_StatusHdr.m_wDestination
	);
	EXPECT_EQ(
		LoadedSettings.m_StatusHdr.m_wSource,
		SavedSettings.m_StatusHdr.m_wSource
	);

	EXPECT_EQ(
		LoadedSettings.m_StatusMsg.m_wType,
		SavedSettings.m_StatusMsg.m_wType
	);

	EXPECT_EQ_1OF2(
		LoadedSettings.m_StatusMsg.m_wDestination,
		SavedSettings.m_StatusMsg.m_wDestination,
		LoadedSettings.m_wPUAddr
	);

	EXPECT_EQ_1OF2(
		LoadedSettings.m_StatusMsg.m_wSource,
		SavedSettings.m_StatusMsg.m_wSource,
		LoadedSettings.m_wCPAddr
	);

	ASSERT_EQ_ARRAYS( LoadedSettings.m_arStatusData, SavedSettings.m_arStatusData );

	EXPECT_EQ( LoadedSettings.m_TUType, SavedSettings.m_TUType );

	EXPECT_EQ( LoadedSettings.m_TUSrcMask, SavedSettings.m_TUSrcMask );
	EXPECT_EQ( LoadedSettings.m_TUSrcComMsgIndex, SavedSettings.m_TUSrcComMsgIndex );
	EXPECT_EQ( LoadedSettings.m_TUPrimToSecSrc, SavedSettings.m_TUPrimToSecSrc );
	EXPECT_EQ( LoadedSettings.m_TUSecToPrimSrc, SavedSettings.m_TUSecToPrimSrc );

	EXPECT_EQ( LoadedSettings.m_bKeepLog, SavedSettings.m_bKeepLog );

	EXPECT_EQ(
		LoadedSettings.m_wLogComposedType,
		SavedSettings.m_wLogComposedType
	);

	if( LoadedSettings.m_bLogUnpackAll != SavedSettings.m_bLogUnpackAll ) {
		EXPECT_EQ( LoadedSettings.m_bLogUnpackAll, SavedSettings.m_bLogUnpackAll );
	}
	else {
		if( LoadedSettings.m_bLogUnpackAll ) {
			EXPECT_EQ(
				LoadedSettings.m_mapLogMsgTypesToUnpack,
				m_DefaultMsgTypeSet
			);
		}
		else {
			EXPECT_EQ(
				SavedSettings.m_mapLogMsgTypesToUnpack,
				LoadedSettings.m_mapLogMsgTypesToUnpack
			);
		}
	}

	EXPECT_EQ(
		LoadedSettings.m_wLogComposedTypeToPack,
		SavedSettings.m_wLogComposedTypeToPack
	);

	if( LoadedSettings.m_bLogPackAll != SavedSettings.m_bLogPackAll ) {
		EXPECT_EQ( LoadedSettings.m_bLogPackAll, SavedSettings.m_bLogPackAll );
	}
	else {
		if( LoadedSettings.m_bLogPackAll ) {
			EXPECT_EQ(
				LoadedSettings.m_mapLogMsgTypesToPack,
				m_DefaultMsgTypeSet
			);
		}
		else {
			EXPECT_EQ(
				SavedSettings.m_mapLogMsgTypesToPack,
				LoadedSettings.m_mapLogMsgTypesToPack
			);
		}
	}

	EXPECT_EQ(
		LoadedSettings.m_wSourceID,
		SavedSettings.m_wSourceID
	);
	EXPECT_EQ(
		LoadedSettings.m_wStatusRequestMessageType,
		SavedSettings.m_wStatusRequestMessageType
	);
}
