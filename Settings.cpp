#include "stdafx.h"
#include "Settings.h"

#include <sstream>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CSettings g_Settings;

CSettings::CSettings()
{
	m_dwPollingPeriod = 1000;
	m_bTestLoopback = FALSE;
	m_bShowSIOMessages = FALSE;
	m_bShowMessageErrors = FALSE;
	m_bShowCOMErrors = FALSE;
	m_strSettingsReportPath = _T("ugs.rep");

	m_nBufferSize = 0x90000;

	m_nIncomingPort = 11112;

	m_strCOMSetup = _T("COM1: baud=9600 data=8 parity=N stop=1");
	m_iCOMRttc = 0;
	m_iCOMWttc = 0;
	m_iCOMRit = -1;

	m_arPrefix.clear();

	m_wComposedType = 0x000003;
	m_wOutputComposedType = 0x0000;
	m_wCRC16Init = 0xFFFF;

	m_wCPAddr = 0x0000;
	m_wPUAddr = 0x0000;

	m_bUnpackAll = FALSE;
	m_bMarkAll = FALSE;

	m_nStatusPeriod = 0;
	m_iSendStatTO = 1000000;
	m_StatusHdr = MESSAGETYPE(0x0000, 0x20);
	m_StatusMsg = MESSAGETYPE(m_wComposedType);
	m_MarkNestedMask = MESSAGETYPE();
	m_MarkComposedMask = MESSAGETYPE();
	m_arStatusData.clear();
	MakeStatusMsg();
	UpdateStatusMsg(0);
	m_TUType = 0x000002;
	m_TUSrcMask = 0x0000;
	m_TUSrcComMsgIndex = FALSE;
	m_TUPrimToSecSrc = 1;
	m_TUSecToPrimSrc = 1;


	m_bKeepLog = FALSE;
	m_wLogComposedType = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wLogComposedTypeToPack = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wSourceID = 0x000020;
	m_wStatusRequestMessageType = 0x0001;

	MESSAGETYPE typeStatus(0x0001, 0x1000);
	m_mapMsgTypes[ 0x0001 ] = typeStatus;
}

CSettings::~CSettings()
{
}

void replDef(unsigned short& expl, const unsigned short& def)
{
  expl = expl ? expl : def;
}


BOOL CSettings::Load(LPCTSTR lpszProfileName)
{
	try
	{
		CStdioFile file(lpszProfileName, CFile::modeRead | CFile::shareDenyWrite | CFile::typeText);

		CMapStringToString mapSettings;
		CString strGroup(_T("[General]"));

		CString strLine;
		while(file.ReadString(strLine))
		{
			strLine.Trim();
			int iComment = strLine.Find(_T(';'), 0);
			if(iComment >= 0)
				strLine.Delete(iComment, strLine.GetLength() - iComment);

			if(strLine.IsEmpty())
				continue;

			if(strLine.Find(_T('[')) == 0 && strLine.ReverseFind(_T(']')) == strLine.GetLength() - 1)
			{
				strGroup = strLine;
				continue;
			}

			int iPos = 0;
			CString strKey(strGroup + _T('/') + strLine.Tokenize(_T("="), iPos).Trim());
			CString strValue(strLine);
			strValue.Delete(0, iPos);

			mapSettings.SetAt(strKey, strValue.Trim().Trim(_T("\"")));
		}

		int iValue;
		if(mapSettings.Lookup(_T("[General]/PollingPeriod"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue) && iValue != 0)
			m_dwPollingPeriod = (DWORD)iValue;

		if(mapSettings.Lookup(_T("[General]/TestLoopback"), strLine))
			m_bTestLoopback = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[General]/ShowSIOMessages"), strLine))
			m_bShowSIOMessages = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[General]/ShowMessageErrors"), strLine))
			m_bShowMessageErrors = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[General]/ShowCOMErrors"), strLine))
			m_bShowCOMErrors = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[General]/SettingsReportPath"), strLine))
			m_strSettingsReportPath = strLine;

		if(mapSettings.Lookup(_T("[General]/BufferSize"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue) && iValue != 0)
			m_nBufferSize = (UINT)iValue;

		if(mapSettings.Lookup(_T("[UDP]/IncomingPort"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue) && iValue != 0)
			m_nIncomingPort = (UINT)iValue;

		if(mapSettings.Lookup(_T("[UDP]/OutgoingIP"), strLine))
		{
			DWORD dwTemp = (DWORD)inet_addr(strLine);
			if(INADDR_NONE == dwTemp)
			{
				LPHOSTENT lphost;
				lphost = gethostbyname(strLine);
				if (lphost != NULL)
					dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
				else
				{
					::WSASetLastError(WSAEINVAL);
					dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
				}
			}

		}


		if(mapSettings.Lookup(_T("[COM]/SetupParams"), strLine))
			m_strCOMSetup = strLine;

		if(mapSettings.Lookup(_T("[COM]/rttc"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_iCOMRttc = iValue;

		if(mapSettings.Lookup(_T("[COM]/wttc"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_iCOMWttc = iValue;

		if(mapSettings.Lookup(_T("[COM]/rit"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_iCOMRit = iValue;

		TByteArray arTemp;
		if(mapSettings.Lookup(_T("[Message]/CPAddr"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wCPAddr = (WORD)iValue;
		if(mapSettings.Lookup(_T("[Message]/PUAddr"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wPUAddr = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/Prefix"), strLine) && ByteArrayFromString(strLine, m_arPrefix, _T("")))
//			m_arPrefix.Copy(arTemp);

		if(mapSettings.Lookup(_T("[Message]/OutPrefix"), strLine) && ByteArrayFromString(strLine, m_arOutPrefix, _T("")))
//			m_arOutPrefix.Copy(arTemp);

		if(mapSettings.Lookup(_T("[Message]/CRC16Init"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wCRC16Init = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/ComposedType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wComposedType = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/OutputComposedType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wOutputComposedType = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/TypesToUnPack"), strLine))
		{
			m_mapMsgTypesToUnpack.clear();
			if(strLine == _T("*"))
			{
				m_mapMsgTypesToUnpack.insert( 0x0000 );
				m_bUnpackAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < strLine.GetLength() - 1; )
				{
					if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
						m_mapMsgTypesToUnpack.insert( static_cast<WORD>(iValue) );
				}
				m_bUnpackAll = FALSE;
			}
		}

		if(mapSettings.Lookup(_T("[Message]/MarkComposedMessageMask"), strLine))
		{
			int iPos = 0;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_MarkComposedMask.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_MarkComposedMask.m_wSrcMask = (WORD)iValue;
		}
		if(mapSettings.Lookup(_T("[Message]/MarkMessageMask"), strLine))
		{
			int iPos = 0;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_MarkNestedMask.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_MarkNestedMask.m_wSrcMask = (WORD)iValue;
		}

		if(mapSettings.Lookup(_T("[Message]/TypesToMark"), strLine))
		{
			m_mapMsgTypesToMark.clear();
			if(strLine == _T("*"))
			{
				m_mapMsgTypesToMark.insert( 0x0000 );
				m_bMarkAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < strLine.GetLength() - 1; )
				{
					if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
						m_mapMsgTypesToMark.insert( static_cast<WORD>(iValue) );
				}
				m_bMarkAll = FALSE;
			}
		}

		m_mapMsgTypes.clear();

		for(int i = 1; i < 10; i++)
		{
			CString strTemp;
			strTemp.Format(_T("[Message]/Type%u"), i);

			if(!mapSettings.Lookup(strTemp, strLine))
				continue;

			MESSAGETYPE type;
			int iPos = 0;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wType = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wMaxLength = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wSource = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wSrcMask = (WORD)iValue;

			if(type.m_wType == 0x0505) {
				replDef(type.m_wSource, m_wPUAddr);
				replDef(type.m_wDestination, m_wCPAddr);
			}
			else if(type.m_wType == 0x0521 || type.m_wType == 0x0532) {
				replDef(type.m_wSource, m_wCPAddr);
			}
			else {
				replDef(type.m_wSource, m_wCPAddr);
				replDef(type.m_wDestination, m_wPUAddr);
			}
			
			m_mapMsgTypes[ type.m_wType] = type;
		}

		MESSAGETYPE typeStatus(0x0001, 0x1000);
		m_mapMsgTypes[ 0x0001 ] = typeStatus;

		if(mapSettings.Lookup(_T("[Message]/StatusPeriod"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_nStatusPeriod = (UINT)iValue;

		if(mapSettings.Lookup(_T("[Message]/SendStatusTO"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_iSendStatTO = (int)iValue;

		if(mapSettings.Lookup(_T("[Message]/StatusMsg"), strLine))
		{
			int iPos = 0;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wType = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wSource = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wType = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wSource = (WORD)iValue;
			ByteArrayFromString(strLine.Tokenize(_T(" "), iPos), m_arStatusData, _T(""));
//			m_arStatusData.Copy(arTemp);

			replDef(m_StatusMsg.m_wSource, m_wCPAddr);
			replDef(m_StatusMsg.m_wDestination, m_wPUAddr);

			MakeStatusMsg();
			UpdateStatusMsg(0);
		}

		if(mapSettings.Lookup(_T("[Message]/TUType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_TUType = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/TUSrcMask"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_TUSrcMask = (WORD)iValue;
		if(mapSettings.Lookup(_T("[Message]/TUSrcComMsgIndex"), strLine))
			m_TUSrcComMsgIndex = strLine.MakeLower() == _T("1") ? TRUE : FALSE;
		if(mapSettings.Lookup(_T("[Message]/TUPrimToSecSrc"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_TUPrimToSecSrc = (UINT)iValue;
		if(mapSettings.Lookup(_T("[Message]/TUSecToPrimSrc"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_TUSecToPrimSrc = (UINT)iValue;

		if(mapSettings.Lookup(_T("[Log]/KeepLog"), strLine))
			m_bKeepLog = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[Log]/LogIP"), strLine))
		{
			DWORD dwTemp = (DWORD)inet_addr(strLine);
			if(INADDR_NONE == dwTemp)
			{
				LPHOSTENT lphost;
				lphost = gethostbyname(strLine);
				if (lphost != NULL)
					dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
				else
				{
					::WSASetLastError(WSAEINVAL);
					dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
				}
			}
		}


		if(mapSettings.Lookup(_T("[Log]/LogComposedType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wLogComposedType = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Log]/LogTypesToUnPack"), strLine))
		{
			m_mapLogMsgTypesToUnpack.clear();
			if(strLine == _T("*"))
			{
				m_mapLogMsgTypesToUnpack.insert( 0x0000 );
				m_bLogUnpackAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < strLine.GetLength() - 1; )
				{
					if (::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
						m_mapLogMsgTypesToUnpack.insert(static_cast<WORD>(iValue));
				}
				m_bLogUnpackAll = FALSE;
			}
		}

		if(mapSettings.Lookup(_T("[Log]/LogComposedTypeToPack"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wLogComposedTypeToPack = (WORD)iValue;


		if(mapSettings.Lookup(_T("[Log]/LogTypesToPack"), strLine))
		{
			m_mapLogMsgTypesToPack.clear();
			if(strLine == _T("*"))
			{
				m_mapLogMsgTypesToPack.insert(0x0000);
				m_bLogPackAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < strLine.GetLength() - 1; )
				{
					if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
						m_mapLogMsgTypesToPack.insert(static_cast<WORD>(iValue));
				}
				m_bLogPackAll = FALSE;
			}
		}

		if(mapSettings.Lookup(_T("[Status]/SourceIndex"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wSourceID = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Status]/StatusRequestMessageType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wStatusRequestMessageType = (WORD)iValue;

		return TRUE;
	}
	catch(CFileException * pfe)
	{
		pfe->Delete();
	}

	return FALSE;
}

template <class T>
std::string FormatHexInt( const T val )
{
	static_assert(std::is_integral<T>::value, "Integral types only!");

	std::ostringstream ss;
	ss << std::hex << std::setfill('0') << std::setw(sizeof(T) * 2) << std::uppercase;
	if (std::is_same<T, unsigned char>() || std::is_same<T, char>()) {
		ss << static_cast<int>(val);
	}
	else {
		ss << val;
	}
	return ss.str();
}

std::ostream & operator << (std::ostream & os, const TByteArray & arr)
{
	for (const auto v : arr) {
		os << FormatHexInt(v);
	}

	return os;
}

bool CSettings::Save(const TString & strProfileName)
{
	std::ofstream f(strProfileName, std::ios::trunc);

	if (!f.good()) {
		return false;
	}

	f << "[General]" << std::endl
		<< "PollingPeriod = "     << m_dwPollingPeriod    << std::endl
		<< "TestLoopback = "      << m_bTestLoopback      << std::endl
		<< "ShowSIOMessages = "   << m_bShowSIOMessages   << std::endl
		<< "ShowMessageErrors = " << m_bShowMessageErrors << std::endl
		<< "ShowCOMErrors = "     << m_bShowCOMErrors     << std::endl
		<< "SettingsReportPath = \"" << m_strSettingsReportPath.GetString() << "\"" << std::endl
		<< "BufferSize = 0x" << FormatHexInt(m_nBufferSize) << std::endl
		;

	f << "[UDP]" << std::endl
		<< "IncomingPort = " << m_nIncomingPort << std::endl
		;

	f << "[COM]" << std::endl
		<< "SetupParams = \"" << m_strCOMSetup << "\"" << std::endl
		<< "rttc = " << m_iCOMRttc << std::endl
		<< "wttc = " << m_iCOMWttc << std::endl
		<< "rit = "  << m_iCOMRit  << std::endl
		;

	f << "[Message]" << std::endl
		<< "CPAddr = 0x" << FormatHexInt(m_wCPAddr) << std::endl
		<< "PUAddr = 0x" << FormatHexInt(m_wPUAddr) << std::endl
		<< "Prefix = \"" << m_arPrefix << "\"" << std::endl
		<< "CRC16Init = 0x" << FormatHexInt(m_wCRC16Init) << std::endl
		<< "ComposedType = 0x" << FormatHexInt(m_wComposedType) << std::endl
		<< "OutputComposedType = 0x" << FormatHexInt(m_wOutputComposedType) << std::endl
		;

	f << "TypesToUnPack = \"";
	if (m_bUnpackAll) {
		f << "*";
	}
	else
	{
		for (const auto v : m_mapMsgTypesToUnpack) {
			f << "0x" << FormatHexInt(v) << " ";
		}
	}
	f << "\"" << std::endl;

	f << "MarkComposedMessageMask = \""
		<< "0x" << FormatHexInt( m_MarkComposedMask.m_wDestMask ) << " "
		<< "0x" << FormatHexInt( m_MarkComposedMask.m_wSrcMask )
		<< "\"" << std::endl
		<< "MarkMessageMask = \""
		<< "0x" << FormatHexInt( m_MarkNestedMask.m_wDestMask ) << " "
		<< "0x" << FormatHexInt( m_MarkNestedMask.m_wSrcMask )
		<< "\"" << std::endl
		;

	f << "TypesToMark = \"";
	if (m_bMarkAll) {
		f << "*";
	}
	else
	{
		for (const auto v : m_mapMsgTypesToMark) {
			f << "0x" << FormatHexInt( v ) << " ";
		}
	}
	f << "\"" << std::endl;
	
	if(m_mapMsgTypes.size() > 0) {
		auto it = m_mapMsgTypes.cbegin();
		int n = 0;
		do {
			f << "Type" << n << " = \""
				<< "0x" << FormatHexInt(it->second.m_wType ) << " "
				<< "0x" << FormatHexInt(it->second.m_wMaxLength) << " "
				<< "0x" << FormatHexInt(it->second.m_wDestination ) << " "
				<< "0x" << FormatHexInt(it->second.m_wSource ) << " "
				<< "0x" << FormatHexInt(it->second.m_wDestMask ) << " "
				<< "0x" << FormatHexInt(it->second.m_wSrcMask )
				<< "\"" << std::endl
			;
		} while (++it != m_mapMsgTypes.end() && ++n < 10);
	}

	f << "StatusPeriod = " << m_nStatusPeriod << std::endl
		<< "SendStatusTO = " << m_iSendStatTO << std::endl
		;

	f << "StatusMsg = \""
		<< "0x" << FormatHexInt( m_StatusHdr.m_wType )      << " "
		<< "0x" << FormatHexInt(m_StatusHdr.m_wDestination) << " "
		<< "0x" << FormatHexInt(m_StatusHdr.m_wSource)      << " "
		<< "0x" << FormatHexInt(m_StatusMsg.m_wType)        << " "
		<< "0x" << FormatHexInt(m_StatusMsg.m_wDestination) << " "
		<< "0x" << FormatHexInt(m_StatusMsg.m_wSource)      << " "
		<< m_arStatusData
		<< "\"" << std::endl
	;

	f << "TUType = 0x" << FormatHexInt( m_TUType ) << std::endl
		<< "TUSrcMask = 0x" << FormatHexInt( m_TUSrcMask ) << std::endl
		<< "TUSrcComMsgIndex = " << m_TUSrcComMsgIndex << std::endl
		<< "TUPrimToSecSrc = " << m_TUPrimToSecSrc << std::endl
		<< "TUSecToPrimSrc = " << m_TUSecToPrimSrc << std::endl
		<< "OutPrefix = \"" << m_arOutPrefix << "\"" << std::endl
	;

	f << "[Log]" << std::endl
		<< "KeepLog = " << m_bKeepLog << std::endl
		<< "LogComposedType = 0x" << FormatHexInt( m_wLogComposedType ) << std::endl
	;

	f << "LogTypesToUnPack = \"";
	if (m_bLogUnpackAll) {
		f << "*";
	}
	else
	{
		for (const auto v : m_mapLogMsgTypesToUnpack) {
			f << "0x" << FormatHexInt(v) << " ";
		}
	}
	f << "\"" << std::endl;

	f << "LogComposedTypeToPack = 0x" << FormatHexInt(m_wLogComposedTypeToPack) << std::endl;

	f << "LogTypesToPack = \"";
	if (m_bLogPackAll) {
		f << "*";
	}
	else
	{
		for (const auto v : m_mapLogMsgTypesToPack) {
			f << "0x" << FormatHexInt(v) << " ";
		}
	}
	f << "\"" << std::endl;

	f << "[Status]" << std::endl
		<< "SourceIndex = 0x" << FormatHexInt( m_wSourceID ) << std::endl
		<< "StatusRequestMessageType = 0x" << FormatHexInt(m_wStatusRequestMessageType) << std::endl
		;

	return true;
}

void CSettings::MakeStatusMsg()
{
	m_StatusMsg.m_wMaxLength = 16 + static_cast<WORD>(m_arStatusData.size());
	m_StatusHdr.m_wMaxLength = 16 + m_StatusMsg.m_wMaxLength;
	m_arStatusMsg.assign(m_StatusHdr.m_wMaxLength, 0);

	WORD * pHeader = reinterpret_cast<WORD *>(m_arStatusMsg.data());
	pHeader[0]  = m_StatusHdr.m_wMaxLength;
	pHeader[1]  = m_StatusHdr.m_wType;
	pHeader[2]  = m_StatusHdr.m_wDestination;
	pHeader[3]  = m_StatusHdr.m_wSource;
	pHeader[7]  = m_StatusMsg.m_wMaxLength;
	pHeader[8]  = m_StatusMsg.m_wType;
	pHeader[9]  = m_StatusMsg.m_wDestination;
	pHeader[10] = m_StatusMsg.m_wSource;

	std::copy(m_arStatusData.cbegin(), m_arStatusData.cend(), m_arStatusMsg.begin() + 28);
}

void CSettings::UpdateStatusMsg(unsigned char ind)
{
	BYTE * pData = m_arStatusMsg.data();
	UINT nLength = static_cast<UINT>(m_arStatusMsg.size());
	*((DWORD *)(pData + 8)) = *((DWORD *)(pData + 22)) = (DWORD)time(NULL);
	pData[12] = ind;
	pData[26] = ind;
	*((WORD *)(pData + nLength - sizeof(DWORD))) = CRC16(pData + 14, nLength - 14 - sizeof(DWORD), m_wCRC16Init);
	*((WORD *)(pData + nLength - sizeof(WORD))) = CRC16(pData, nLength - sizeof(WORD), m_wCRC16Init);
}

//----------------------------------------------------------------------------

const char * CSettings::GetSettingsReportPath( void ) const
{
	return m_strSettingsReportPath.GetString();
}

unsigned int CSettings::GetBufferSize( void ) const
{
	return m_nBufferSize;
}

unsigned long CSettings::GetPollingPeriod( void ) const
{
	return m_dwPollingPeriod;
}

WORD CSettings::GetCRC16Init( void ) const
{
	return this->m_wCRC16Init;
}

const CSettings::MESSAGETYPE & CSettings::GetStatusHdr( void ) const
{
	return m_StatusHdr;
}

const CSettings::MESSAGETYPE & CSettings::GetStatusMessageHdr( void ) const
{
	return m_StatusMsg;
}

void CSettings::SetStatusData( const TByteArray & rData )
{
	m_arStatusData = rData;
}

const TByteArray & CSettings::GetStatusData( void ) const
{
	return m_arStatusData;
}

void CSettings::SetStatusMessageData(const TByteArray & rData)
{
	m_arStatusMsg = rData;
}

const TByteArray & CSettings::GetStatusMessageData(void) const
{
	return m_arStatusMsg;
}

