#ifndef __CMNDEFS_H__
#define __CMNDEFS_H__

#include <vector>
#include <map>
#include <set>
#include <string>
#include <fstream>
#include <iomanip>


//namespace UGS {

using TByteArray  = std::vector< BYTE >;

using TMsgTypeKey = WORD;
using TMsgTypeSet = std::set< TMsgTypeKey >;

#if defined(UNICODE) || defined(_UNICODE)
	using TString = std::wstring;
#else   // defined(UNICODE) || defined(_UNICODE)
	using TString = std::string;
#endif  // defined(UNICODE) || defined(_UNICODE)

//} // namespace UGS

#endif // __CMNDEFS_H__
